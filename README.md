Jeecg-Boot-Pro 低代码开发平台
===============

## 修改说明 ：

- 本项目基于jeecg-boot 3.0进行重新分包修改
    - 去除了第三方登陆依赖。
    - 去除了微服务模块代码。
    - 去除了docker相关代码。

- Admin端采用 shiro + jwt
- App端采用 sa-token 官网：[https://sa-token.dev33.cn](https://sa-token.dev33.cn)
- 可根据自己需求进行相关模块开发，更加方便进行依赖管理

##### 项目结构图：

```
── jeecg-boot-pro
├── jeecg-boot-admin                      // 后台管理启动模块
├── jeecg-boot-app                        // APP端管理模块
├── jeecg-boot-common                     // [依赖] 系统公共模块
├── jeecg-boot-module-base                // [依赖] 基础模块
├── jeecg-boot-module-message             // [依赖] 消息模块
├── jeecg-boot-module-system              // [依赖] 系统模块
├── jeecg-boot-pro-ui                     // 前端ui
├──pom.xml                                // [依赖] 顶级pom文件 

```

## 后端技术架构

- 基础框架：Spring Boot 2.3.5.RELEASE

- 持久层框架：Mybatis-plus 3.4.3.2

- 安全框架：Apache Shiro 1.7.0，Jwt 3.11.0 ，sa-token 1.28.0

- 数据库连接池：阿里巴巴Druid 1.1.22

- 缓存框架：redis

- 日志打印：logback

- 其他：fastjson，poi，Swagger-ui，quartz, lombok（简化代码）等。

- 日志框架： TLog （TLog是一个轻量级的分布式日志标记追踪神器）

## 开发环境

- 语言：Java 8

- IDE(JAVA)： Eclipse安装lombok插件 或者 IDEA

- 依赖管理：Maven

- 数据库：MySQL5.7+ & Oracle 11g & SqlServer & postgresql & 国产等更多数据库

- 缓存：Redis

## 技术文档

- 在线文档：  [http://doc.jeecg.com](http://doc.jeecg.com)

- 常见问题：  [http://jeecg.com/doc/qa](http://jeecg.com/doc/qa)

## UI运行
----

- 安装依赖

```
yarn install
```

- 开发模式运行

```
yarn run serve
```

- 编译项目

```
yarn run build
```

- Lints and fixes files

```
yarn run lint
```

## 计划 :

1. 引入工作流模块（可插拔）
2. 修复内相关规范代码 （根据阿里巴巴规范进行修复）
