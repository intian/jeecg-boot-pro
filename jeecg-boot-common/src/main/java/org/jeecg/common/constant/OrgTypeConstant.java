package org.jeecg.common.constant;

/**
 * @Author werdor
 * @Date 2022/1/6 11:40 上午
 **/
public class OrgTypeConstant {

    /**
     * 1：企业
     */
    public static final Integer ORG_TYPE_1 = 1;

    /**
     * 2：专家
     */
    public static final Integer ORG_TYPE_2 = 2;

    /**
     * 3：推荐单位
     */
    public static final Integer ORG_TYPE_3 = 3;

    /**
     * 4：企业个人
     */
    public static final Integer ORG_TYPE_4 = 4;
}
