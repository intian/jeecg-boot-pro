package org.jeecg.common.util;

import org.jeecg.common.exception.JeecgBootException;

import javax.xml.bind.PropertyException;
import java.util.List;
import java.util.Map;

/**
 * @Author werdor
 * @Date 2022/1/6 11:28 上午
 **/
public class DocumentUtils {

    /**
     * 验证必须字段是否存在，若不存在抛PropertyException
     *
     * @param doc
     * @param requiredfileds
     */
    public static void verifyRequiredField(Map doc, List<String> requiredfileds) {
        for (String field : requiredfileds) {
            if (!doc.containsKey(field)) {
                throw new JeecgBootException("当前不存在必要字段");
            }
        }
    }
}
