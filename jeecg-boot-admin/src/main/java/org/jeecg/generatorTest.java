package org.jeecg;


import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;

import java.util.Collections;

/**
 * @Author werdor
 * @Date 2022/1/5 11:23 上午
 **/
public class generatorTest {

    public static void main(String[] args) {
        FastAutoGenerator.create("jdbc:mysql://localhost:3306/kjj?useUnicode=true&characterEncoding=UTF-8", "root", "12345678")
                .globalConfig(builder -> {
                    builder.author("weirdor") // 设置作者
                            .fileOverride() // 覆盖已生成文件
                            .outputDir("/Users/werdor/Desktop/jeecg-boot-pro/jeecg-boot-module-share/src/main/java"); // 指定输出目录
                })
                .packageConfig(builder -> {
                    builder.parent("org.jeecg.modules.share") // 设置父包名
                            .moduleName("umsExpert") // 设置父包模块名
                            .pathInfo(Collections.singletonMap(OutputFile.mapperXml, "/Users/werdor/Desktop/jeecg-boot-pro/jeecg-boot-module-share/src/main/resources/mapper")); // 设置mapperXml生成路径
                })
                .strategyConfig(builder -> {
                    builder.addInclude("ums_expert") // 设置需要生成的表名
                            .addTablePrefix("t_", "c_"); // 设置过滤表前缀
                })
                .templateEngine(new FreemarkerTemplateEngine()) // 使用Freemarker引擎模板，默认的是Velocity引擎模板
                .execute();

    }
}
