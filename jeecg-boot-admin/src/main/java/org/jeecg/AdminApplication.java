package org.jeecg;

import com.yomahub.tlog.core.enhance.bytes.AspectLogEnhance;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.admin.context.listener.ApplicationReadyEventListener;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 单体启动类（采用此类启动为单体模式）
 *
 * @author werdor
 */
@Slf4j
@SpringBootApplication
public class AdminApplication{

    //进行日志增强，自动判断日志框架
    static {
        AspectLogEnhance.enhance();
    }

    public static void main(String[] args) {
        System.out.println("***************************************************************");
        System.out.println("启动adminBackend...");

        SpringApplication app = new SpringApplication(AdminApplication.class);
        app.addListeners(new ApplicationReadyEventListener());
        app.run(args);
    }

}