package org.jeecg.admin.modules.oss.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.admin.modules.oss.entity.OSSFile;

public interface OSSFileMapper extends BaseMapper<OSSFile> {

}
