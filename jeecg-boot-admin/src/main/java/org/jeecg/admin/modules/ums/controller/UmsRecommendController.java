package org.jeecg.admin.modules.ums.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.share.umsRecommend.entity.UmsRecommend;
import org.jeecg.modules.share.umsRecommend.service.IUmsRecommendService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;

/**
* @Description: ums_recommend
* @Author: jeecg-boot
* @Date:   2022-01-19
* @Version: V1.0
*/
@RestController
@RequestMapping("/umsRecommend/umsRecommend")
@Slf4j
public class UmsRecommendController extends JeecgController<UmsRecommend, IUmsRecommendService> {
   @Autowired
   private IUmsRecommendService umsRecommendService;

   /**
    * 分页列表查询
    *
    * @param umsRecommend
    * @param pageNo
    * @param pageSize
    * @param req
    * @return
    */
   @AutoLog(value = "ums_recommend-分页列表查询")
   @GetMapping(value = "/list")
   public Result<?> queryPageList(UmsRecommend umsRecommend,
                                  @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
                                  @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
                                  HttpServletRequest req) {
       QueryWrapper<UmsRecommend> queryWrapper = QueryGenerator.initQueryWrapper(umsRecommend, req.getParameterMap());
       Page<UmsRecommend> page = new Page<UmsRecommend>(pageNo, pageSize);
       IPage<UmsRecommend> pageList = umsRecommendService.page(page, queryWrapper);
       return Result.OK(pageList);
   }

   /**
    *   添加
    *
    * @param umsRecommend
    * @return
    */
   @AutoLog(value = "ums_recommend-添加")
   @PostMapping(value = "/add")
   public Result<?> add(@RequestBody UmsRecommend umsRecommend) {
       umsRecommendService.save(umsRecommend);
       return Result.OK("添加成功！");
   }

   /**
    *  编辑
    *
    * @param umsRecommend
    * @return
    */
   @AutoLog(value = "ums_recommend-编辑")
   @PutMapping(value = "/edit")
   public Result<?> edit(@RequestBody UmsRecommend umsRecommend) {
       umsRecommendService.updateById(umsRecommend);
       return Result.OK("编辑成功!");
   }

   /**
    *   通过id删除
    *
    * @param id
    * @return
    */
   @AutoLog(value = "ums_recommend-通过id删除")
   @DeleteMapping(value = "/delete")
   public Result<?> delete(@RequestParam(name="id",required=true) String id) {
       umsRecommendService.removeById(id);
       return Result.OK("删除成功!");
   }

   /**
    *  批量删除
    *
    * @param ids
    * @return
    */
   @AutoLog(value = "ums_recommend-批量删除")
   @DeleteMapping(value = "/deleteBatch")
   public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
       this.umsRecommendService.removeByIds(Arrays.asList(ids.split(",")));
       return Result.OK("批量删除成功!");
   }

   /**
    * 通过id查询
    *
    * @param id
    * @return
    */
   @AutoLog(value = "ums_recommend-通过id查询")
   @GetMapping(value = "/queryById")
   public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
       UmsRecommend umsRecommend = umsRecommendService.getById(id);
       if(umsRecommend==null) {
           return Result.error("未找到对应数据");
       }
       return Result.OK(umsRecommend);
   }

   /**
   * 导出excel
   *
   * @param request
   * @param umsRecommend
   */
   @RequestMapping(value = "/exportXls")
   public ModelAndView exportXls(HttpServletRequest request, UmsRecommend umsRecommend) {
       return super.exportXls(request, umsRecommend, UmsRecommend.class, "ums_recommend");
   }

   /**
     * 通过excel导入数据
   *
   * @param request
   * @param response
   * @return
   */
   @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
   public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
       return super.importExcel(request, response, UmsRecommend.class);
   }

}
