package org.jeecg.admin.modules.ums.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.share.umsUser.entity.UmsUser;
import org.jeecg.modules.share.umsUser.service.IUmsUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;

/**
* @Description: ums_user
* @Author: jeecg-boot
* @Date:   2022-01-05
* @Version: V1.0
*/
@RestController
@RequestMapping("/admin/umsUser")
@Slf4j
public class AdminUserController extends JeecgController<UmsUser, IUmsUserService> {
   @Autowired
   private IUmsUserService umsUserService;

   /**
    * 分页列表查询
    *
    * @param umsUser
    * @param pageNo
    * @param pageSize
    * @param req
    * @return
    */
   @AutoLog(value = "ums_user-分页列表查询")
   @GetMapping(value = "/list")
   public Result<?> queryPageList(UmsUser umsUser,
                                  @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
                                  @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
                                  HttpServletRequest req) {
       QueryWrapper<UmsUser> queryWrapper = QueryGenerator.initQueryWrapper(umsUser, req.getParameterMap());
       Page<UmsUser> page = new Page<UmsUser>(pageNo, pageSize);
       IPage<UmsUser> pageList = umsUserService.page(page, queryWrapper);
       return Result.OK(pageList);
   }

   /**
    *   添加
    *
    * @param umsUser
    * @return
    */
   @AutoLog(value = "ums_user-添加")
   @PostMapping(value = "/add")
   public Result<?> add(@RequestBody UmsUser umsUser) {
       umsUserService.save(umsUser);
       return Result.OK("添加成功！");
   }

   /**
    *  编辑
    *
    * @param umsUser
    * @return
    */
   @AutoLog(value = "ums_user-编辑")
   @PutMapping(value = "/edit")
   public Result<?> edit(@RequestBody UmsUser umsUser) {
       umsUserService.updateById(umsUser);
       return Result.OK("编辑成功!");
   }

   /**
    *   通过id删除
    *
    * @param id
    * @return
    */
   @AutoLog(value = "ums_user-通过id删除")
   @DeleteMapping(value = "/delete")
   public Result<?> delete(@RequestParam(name="id",required=true) String id) {
       umsUserService.removeById(id);
       return Result.OK("删除成功!");
   }

   /**
    *  批量删除
    *
    * @param ids
    * @return
    */
   @AutoLog(value = "ums_user-批量删除")
   @DeleteMapping(value = "/deleteBatch")
   public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
       this.umsUserService.removeByIds(Arrays.asList(ids.split(",")));
       return Result.OK("批量删除成功!");
   }

   /**
    * 通过id查询
    *
    * @param id
    * @return
    */
   @AutoLog(value = "ums_user-通过id查询")
   @GetMapping(value = "/queryById")
   public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
       UmsUser umsUser = umsUserService.getById(id);
       if(umsUser==null) {
           return Result.error("未找到对应数据");
       }
       return Result.OK(umsUser);
   }

   /**
   * 导出excel
   *
   * @param request
   * @param umsUser
   */
   @RequestMapping(value = "/exportXls")
   public ModelAndView exportXls(HttpServletRequest request, UmsUser umsUser) {
       return super.exportXls(request, umsUser, UmsUser.class, "ums_user");
   }

   /**
     * 通过excel导入数据
   *
   * @param request
   * @param response
   * @return
   */
   @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
   public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
       return super.importExcel(request, response, UmsUser.class);
   }

}
