package org.jeecg.admin.modules.ums.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.share.umsProjectCategory.entity.UmsProjectCategory;
import org.jeecg.modules.share.umsProjectCategory.service.IUmsProjectCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;

/**
* @Description: ums_project_category
* @Author: jeecg-boot
* @Date:   2022-01-19
* @Version: V1.0
*/
@RestController
@RequestMapping("/umsProjectCategory/umsProjectCategory")
@Slf4j
public class UmsProjectCategoryController extends JeecgController<UmsProjectCategory, IUmsProjectCategoryService> {
   @Autowired
   private IUmsProjectCategoryService umsProjectCategoryService;

   /**
    * 分页列表查询
    *
    * @param umsProjectCategory
    * @param pageNo
    * @param pageSize
    * @param req
    * @return
    */
   @AutoLog(value = "ums_project_category-分页列表查询")
   @GetMapping(value = "/list")
   public Result<?> queryPageList(UmsProjectCategory umsProjectCategory,
                                  @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
                                  @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
                                  HttpServletRequest req) {
       QueryWrapper<UmsProjectCategory> queryWrapper = QueryGenerator.initQueryWrapper(umsProjectCategory, req.getParameterMap());
       Page<UmsProjectCategory> page = new Page<UmsProjectCategory>(pageNo, pageSize);
       IPage<UmsProjectCategory> pageList = umsProjectCategoryService.page(page, queryWrapper);
       return Result.OK(pageList);
   }

   /**
    *   添加
    *
    * @param umsProjectCategory
    * @return
    */
   @AutoLog(value = "ums_project_category-添加")
   @PostMapping(value = "/add")
   public Result<?> add(@RequestBody UmsProjectCategory umsProjectCategory) {
       umsProjectCategoryService.save(umsProjectCategory);
       return Result.OK("添加成功！");
   }

   /**
    *  编辑
    *
    * @param umsProjectCategory
    * @return
    */
   @AutoLog(value = "ums_project_category-编辑")
   @PutMapping(value = "/edit")
   public Result<?> edit(@RequestBody UmsProjectCategory umsProjectCategory) {
       umsProjectCategoryService.updateById(umsProjectCategory);
       return Result.OK("编辑成功!");
   }

   /**
    *   通过id删除
    *
    * @param id
    * @return
    */
   @AutoLog(value = "ums_project_category-通过id删除")
   @DeleteMapping(value = "/delete")
   public Result<?> delete(@RequestParam(name="id",required=true) String id) {
       umsProjectCategoryService.removeById(id);
       return Result.OK("删除成功!");
   }

   /**
    *  批量删除
    *
    * @param ids
    * @return
    */
   @AutoLog(value = "ums_project_category-批量删除")
   @DeleteMapping(value = "/deleteBatch")
   public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
       this.umsProjectCategoryService.removeByIds(Arrays.asList(ids.split(",")));
       return Result.OK("批量删除成功!");
   }

   /**
    * 通过id查询
    *
    * @param id
    * @return
    */
   @AutoLog(value = "ums_project_category-通过id查询")
   @GetMapping(value = "/queryById")
   public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
       UmsProjectCategory umsProjectCategory = umsProjectCategoryService.getById(id);
       if(umsProjectCategory==null) {
           return Result.error("未找到对应数据");
       }
       return Result.OK(umsProjectCategory);
   }

   /**
   * 导出excel
   *
   * @param request
   * @param umsProjectCategory
   */
   @RequestMapping(value = "/exportXls")
   public ModelAndView exportXls(HttpServletRequest request, UmsProjectCategory umsProjectCategory) {
       return super.exportXls(request, umsProjectCategory, UmsProjectCategory.class, "ums_project_category");
   }

   /**
     * 通过excel导入数据
   *
   * @param request
   * @param response
   * @return
   */
   @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
   public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
       return super.importExcel(request, response, UmsProjectCategory.class);
   }

}
