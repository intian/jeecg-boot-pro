package org.jeecg.admin.modules.ums.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.share.umsExpert.entity.UmsExpert;
import org.jeecg.modules.share.umsExpert.service.IUmsExpertService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;

/**
* @Description: ums_expert
* @Author: jeecg-boot
* @Date:   2022-01-19
* @Version: V1.0
*/
@RestController
@RequestMapping("/umsExpert/umsExpert")
@Slf4j
public class UmsExpertController extends JeecgController<UmsExpert, IUmsExpertService> {
   @Autowired
   private IUmsExpertService umsExpertService;

   /**
    * 分页列表查询
    *
    * @param umsExpert
    * @param pageNo
    * @param pageSize
    * @param req
    * @return
    */
   @AutoLog(value = "ums_expert-分页列表查询")
   @GetMapping(value = "/list")
   public Result<?> queryPageList(UmsExpert umsExpert,
                                  @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
                                  @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
                                  HttpServletRequest req) {
       QueryWrapper<UmsExpert> queryWrapper = QueryGenerator.initQueryWrapper(umsExpert, req.getParameterMap());
       Page<UmsExpert> page = new Page<UmsExpert>(pageNo, pageSize);
       IPage<UmsExpert> pageList = umsExpertService.page(page, queryWrapper);
       return Result.OK(pageList);
   }

   /**
    *   添加
    *
    * @param umsExpert
    * @return
    */
   @AutoLog(value = "ums_expert-添加")
   @PostMapping(value = "/add")
   public Result<?> add(@RequestBody UmsExpert umsExpert) {
       umsExpertService.save(umsExpert);
       return Result.OK("添加成功！");
   }

   /**
    *  编辑
    *
    * @param umsExpert
    * @return
    */
   @AutoLog(value = "ums_expert-编辑")
   @PutMapping(value = "/edit")
   public Result<?> edit(@RequestBody UmsExpert umsExpert) {
       umsExpertService.updateById(umsExpert);
       return Result.OK("编辑成功!");
   }

   /**
    *   通过id删除
    *
    * @param id
    * @return
    */
   @AutoLog(value = "ums_expert-通过id删除")
   @DeleteMapping(value = "/delete")
   public Result<?> delete(@RequestParam(name="id",required=true) String id) {
       umsExpertService.removeById(id);
       return Result.OK("删除成功!");
   }

   /**
    *  批量删除
    *
    * @param ids
    * @return
    */
   @AutoLog(value = "ums_expert-批量删除")
   @DeleteMapping(value = "/deleteBatch")
   public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
       this.umsExpertService.removeByIds(Arrays.asList(ids.split(",")));
       return Result.OK("批量删除成功!");
   }

   /**
    * 通过id查询
    *
    * @param id
    * @return
    */
   @AutoLog(value = "ums_expert-通过id查询")
   @GetMapping(value = "/queryById")
   public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
       UmsExpert umsExpert = umsExpertService.getById(id);
       if(umsExpert==null) {
           return Result.error("未找到对应数据");
       }
       return Result.OK(umsExpert);
   }

   /**
   * 导出excel
   *
   * @param request
   * @param umsExpert
   */
   @RequestMapping(value = "/exportXls")
   public ModelAndView exportXls(HttpServletRequest request, UmsExpert umsExpert) {
       return super.exportXls(request, umsExpert, UmsExpert.class, "ums_expert");
   }

   /**
     * 通过excel导入数据
   *
   * @param request
   * @param response
   * @return
   */
   @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
   public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
       return super.importExcel(request, response, UmsExpert.class);
   }

}
