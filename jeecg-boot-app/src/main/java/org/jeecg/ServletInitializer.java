package org.jeecg;


import org.jeecg.app.context.listener.ApplicationReadyEventListener;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;


/**
 * @Author werdor
 * @Date 2022/1/4 4:41 下午
 **/
public class ServletInitializer extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {

        System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
        System.out.println("启动api...");

        application.sources(AppAppilcation.class)
                .listeners(new ApplicationReadyEventListener());
        return application;

    }

}