package org.jeecg;

import com.yomahub.tlog.core.enhance.bytes.AspectLogEnhance;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.app.context.listener.ApplicationReadyEventListener;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * @Author werdor
 * @Date 2021/11/16 3:50 下午
 **/
@Slf4j
@EnableAsync
@SpringBootApplication
public class AppAppilcation {

    //进行日志增强，自动判断日志框架
    static {
        AspectLogEnhance.enhance();
    }

    public static void main(String[] args) {
        System.out.println("***************************************************************");
        System.out.println("启动Api...");

        SpringApplication app = new SpringApplication(AppAppilcation.class);
        app.addListeners(new ApplicationReadyEventListener());
        app.run(args);
    }

}