package org.jeecg.app.modules.oss.controller;

import org.jeecg.common.api.vo.Result;
import org.jeecg.common.util.MinioUtil;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;


/**
 * 文件管理
 *
 * @author werdor
 */
@RestController
@RequestMapping("/file")
public class FileController {

    @PostMapping("/upload")
    public Result<?> upload(@RequestParam MultipartFile file) {
        String files = MinioUtil.upload(file, "");
        return Result.OK("上传成功", files);

    }
}
