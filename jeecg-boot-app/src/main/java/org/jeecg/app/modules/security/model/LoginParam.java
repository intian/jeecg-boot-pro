package org.jeecg.app.modules.security.model;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @Author werdor
 * @Date 2022/1/5 11:40 上午
 **/
@Data
public class LoginParam {

    /**
     * 用户名
     */
    @NotBlank(message = "用户名参数缺失")
    private String username;

    /**
     * 密码
     */
    @NotBlank(message = "密码参数缺失")
    private String password;

    /**
     * 用户类型 1.企业 2.专家
     */
    @NotBlank(message = "用户类型缺失")
    private Integer userType;

    /**
     * 验证码
     */
    private String captcha;

    /**
     * 验证码key
     */
    private String checkKey;

}
