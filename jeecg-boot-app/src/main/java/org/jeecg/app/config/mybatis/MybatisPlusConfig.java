package org.jeecg.app.config.mybatis;

import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * MybatisPlus配置
 * Created by wangfan on 2018-02-22 11:29
 *
 * @author werdor
 */
@EnableTransactionManagement
@Configuration
@MapperScan(value = {"org.jeecg.modules.**.mapper*", "org.jeecg.*.modules.**.mapper*"})
public class MybatisPlusConfig {

    @Bean
    public MybatisPlusInterceptor paginationInterceptor() {
        MybatisPlusInterceptor paginationInterceptor = new MybatisPlusInterceptor();
        PaginationInnerInterceptor page = new PaginationInnerInterceptor();
        paginationInterceptor.addInnerInterceptor(page);
        return paginationInterceptor;
    }


}