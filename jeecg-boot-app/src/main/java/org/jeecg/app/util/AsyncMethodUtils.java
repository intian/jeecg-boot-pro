package org.jeecg.app.util;

import cn.hutool.extra.mail.MailUtil;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.List;

@Async
@Component
public class AsyncMethodUtils {


    public void sendEmail(String receiver, String subject, String content) {
        MailUtil.send(receiver, subject, content, false);
    }

    public void sendHtmlEmail(String receivers, String subject, String content) {
        MailUtil.send(receivers, subject, content, true,null);
    }
}
