package org.jeecg.modules.share.umsExpert.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: ums_expert
 * @Author: jeecg-boot
 * @Date:   2022-01-19
 * @Version: V1.0
 */
@Data
@TableName("ums_expert")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="ums_expert对象", description="ums_expert")
public class UmsExpert implements Serializable {
    private static final long serialVersionUID = 1L;

	/**ID*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "ID")
    private java.lang.String id;
	/**专家姓名*/
	@Excel(name = "专家姓名", width = 15)
    @ApiModelProperty(value = "专家姓名")
    private java.lang.String realname;
	/**性别*/
	@Excel(name = "性别", width = 15)
    @ApiModelProperty(value = "性别")
    private java.lang.Integer sex;
	/**头像*/
	@Excel(name = "头像", width = 15)
    @ApiModelProperty(value = "头像")
    private java.lang.String avatar;
	/**单位性质*/
	@Excel(name = "单位性质", width = 15)
    @ApiModelProperty(value = "单位性质")
    private java.lang.String orgNature;
	/**办公地点*/
	@Excel(name = "办公地点", width = 15)
    @ApiModelProperty(value = "办公地点")
    private java.lang.String orgAddress;
	/**办公电话*/
	@Excel(name = "办公电话", width = 15)
    @ApiModelProperty(value = "办公电话")
    private java.lang.String orgTel;
	/**职务*/
	@Excel(name = "职务", width = 15)
    @ApiModelProperty(value = "职务")
    private java.lang.String jobs;
	/**职称*/
	@Excel(name = "职称", width = 15)
    @ApiModelProperty(value = "职称")
    private java.lang.String post;
	/**邮箱*/
	@Excel(name = "邮箱", width = 15)
    @ApiModelProperty(value = "邮箱")
    private java.lang.String email;
	/**手机号*/
	@Excel(name = "手机号", width = 15)
    @ApiModelProperty(value = "手机号")
    private java.lang.String phone;
	/**证件号码*/
	@Excel(name = "证件号码", width = 15)
    @ApiModelProperty(value = "证件号码")
    private java.lang.String idCard;
	/**身份类型*/
	@Excel(name = "身份类型", width = 15)
    @ApiModelProperty(value = "身份类型")
    private java.lang.Integer idType;
	/**学历
*/
	@Excel(name = "学历
", width = 15)
    @ApiModelProperty(value = "学历
")
    private java.lang.String education;
	/**学位*/
	@Excel(name = "学位", width = 15)
    @ApiModelProperty(value = "学位")
    private java.lang.String degree;
	/**单位名称
*/
	@Excel(name = "单位名称
", width = 15)
    @ApiModelProperty(value = "单位名称
")
    private java.lang.String orgName;
	/**籍贯*/
	@Excel(name = "籍贯", width = 15)
    @ApiModelProperty(value = "籍贯")
    private java.lang.String nativePlace;
	/**专家类型
*/
	@Excel(name = "专家类型
", width = 15)
    @ApiModelProperty(value = "专家类型
")
    private java.lang.String expertType;
	/**研究方向
*/
	@Excel(name = "研究方向
", width = 15)
    @ApiModelProperty(value = "研究方向
")
    private java.lang.String researchDirection;
	/**开户银行
*/
	@Excel(name = "开户银行
", width = 15)
    @ApiModelProperty(value = "开户银行
")
    private java.lang.String bankName;
	/**开户账号
*/
	@Excel(name = "开户账号
", width = 15)
    @ApiModelProperty(value = "开户账号
")
    private java.lang.String bankAccount;
	/**个人履历
*/
	@Excel(name = "个人履历
", width = 15)
    @ApiModelProperty(value = "个人履历
")
    private java.lang.String resume;
	/**研究领域
*/
	@Excel(name = "研究领域
", width = 15)
    @ApiModelProperty(value = "研究领域
")
    private java.lang.String researchField;
	/**专业关键词
*/
	@Excel(name = "专业关键词
", width = 15)
    @ApiModelProperty(value = "专业关键词
")
    private java.lang.String majorKeyword;
	/**参与评审兴趣度
*/
	@Excel(name = "参与评审兴趣度
", width = 15)
    @ApiModelProperty(value = "参与评审兴趣度
")
    private java.lang.String reviewScore;
	/**研究成果
*/
	@Excel(name = "研究成果
", width = 15)
    @ApiModelProperty(value = "研究成果
")
    private java.lang.String researchResult;
	/**承担项目情况
*/
	@Excel(name = "承担项目情况
", width = 15)
    @ApiModelProperty(value = "承担项目情况
")
    private java.lang.String projectResult;
	/**审核状态
*/
	@Excel(name = "审核状态
", width = 15)
    @ApiModelProperty(value = "审核状态
")
    private java.lang.Integer status;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建时间")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新时间")
    private java.util.Date updateTime;
	/**删除标记*/
	@Excel(name = "删除标记", width = 15)
    @ApiModelProperty(value = "删除标记")
    private java.lang.Integer delFlag;
}
