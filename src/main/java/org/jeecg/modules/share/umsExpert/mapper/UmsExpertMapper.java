package org.jeecg.modules.share.umsExpert.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.share.umsExpert.entity.UmsExpert;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: ums_expert
 * @Author: jeecg-boot
 * @Date:   2022-01-19
 * @Version: V1.0
 */
public interface UmsExpertMapper extends BaseMapper<UmsExpert> {

}
