package org.jeecg.modules.share.umsExpert.service.impl;

import org.jeecg.modules.share.umsExpert.entity.UmsExpert;
import org.jeecg.modules.share.umsExpert.mapper.UmsExpertMapper;
import org.jeecg.modules.share.umsExpert.service.IUmsExpertService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: ums_expert
 * @Author: jeecg-boot
 * @Date:   2022-01-19
 * @Version: V1.0
 */
@Service
public class UmsExpertServiceImpl extends ServiceImpl<UmsExpertMapper, UmsExpert> implements IUmsExpertService {

}
