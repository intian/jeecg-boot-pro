package org.jeecg.modules.share.umsRecommend.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.share.umsRecommend.entity.UmsRecommend;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: ums_recommend
 * @Author: jeecg-boot
 * @Date:   2022-01-19
 * @Version: V1.0
 */
public interface UmsRecommendMapper extends BaseMapper<UmsRecommend> {

}
