package org.jeecg.modules.share.umsRecommend.service;

import org.jeecg.modules.share.umsRecommend.entity.UmsRecommend;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: ums_recommend
 * @Author: jeecg-boot
 * @Date:   2022-01-19
 * @Version: V1.0
 */
public interface IUmsRecommendService extends IService<UmsRecommend> {

}
