package org.jeecg.modules.share.umsEnterprise.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: ums_enterprise
 * @Author: jeecg-boot
 * @Date:   2022-01-19
 * @Version: V1.0
 */
@Data
@TableName("ums_enterprise")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="ums_enterprise对象", description="ums_enterprise")
public class UmsEnterprise implements Serializable {
    private static final long serialVersionUID = 1L;

	/**ID*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "ID")
    private java.lang.String id;
	/**企业姓名*/
	@Excel(name = "企业姓名", width = 15)
    @ApiModelProperty(value = "企业姓名")
    private java.lang.String orgName;
	/**统一信用代码*/
	@Excel(name = "统一信用代码", width = 15)
    @ApiModelProperty(value = "统一信用代码")
    private java.lang.String creditCode;
	/**法人姓名*/
	@Excel(name = "法人姓名", width = 15)
    @ApiModelProperty(value = "法人姓名")
    private java.lang.String legalName;
	/**电子邮箱*/
	@Excel(name = "电子邮箱", width = 15)
    @ApiModelProperty(value = "电子邮箱")
    private java.lang.String email;
	/**联系电话*/
	@Excel(name = "联系电话", width = 15)
    @ApiModelProperty(value = "联系电话")
    private java.lang.String phone;
	/**推荐单位*/
	@Excel(name = "推荐单位", width = 15)
    @ApiModelProperty(value = "推荐单位")
    private java.lang.String recommendOrg;
	/**注册类型*/
	@Excel(name = "注册类型", width = 15)
    @ApiModelProperty(value = "注册类型")
    private java.lang.String registerType;
	/**注册资本*/
	@Excel(name = "注册资本", width = 15)
    @ApiModelProperty(value = "注册资本")
    private java.lang.String registerCapital;
	/**注册时间*/
	@Excel(name = "注册时间", width = 15)
    @ApiModelProperty(value = "注册时间")
    private java.lang.String registerTime;
	/**单位地址*/
	@Excel(name = "单位地址", width = 15)
    @ApiModelProperty(value = "单位地址")
    private java.lang.String address;
	/**邮政编码*/
	@Excel(name = "邮政编码", width = 15)
    @ApiModelProperty(value = "邮政编码")
    private java.lang.String postalCode;
	/**网址*/
	@Excel(name = "网址", width = 15)
    @ApiModelProperty(value = "网址")
    private java.lang.String website;
	/**单位性质*/
	@Excel(name = "单位性质", width = 15)
    @ApiModelProperty(value = "单位性质")
    private java.lang.String orgNature;
	/**传真*/
	@Excel(name = "传真", width = 15)
    @ApiModelProperty(value = "传真")
    private java.lang.String fax;
	/**是否为高新企业*/
	@Excel(name = "是否为高新企业", width = 15)
    @ApiModelProperty(value = "是否为高新企业")
    private java.lang.Integer isHighTech;
	/**职工人数*/
	@Excel(name = "职工人数", width = 15)
    @ApiModelProperty(value = "职工人数")
    private java.lang.String num;
	/**高新企业批文号*/
	@Excel(name = "高新企业批文号", width = 15)
    @ApiModelProperty(value = "高新企业批文号")
    private java.lang.String highTechNo;
	/**高新企业批文时间*/
	@Excel(name = "高新企业批文时间", width = 15)
    @ApiModelProperty(value = "高新企业批文时间")
    private java.lang.String highTechTime;
	/**所属行业*/
	@Excel(name = "所属行业", width = 15)
    @ApiModelProperty(value = "所属行业")
    private java.lang.String industry;
	/**龙头企业发证单位*/
	@Excel(name = "龙头企业发证单位", width = 15)
    @ApiModelProperty(value = "龙头企业发证单位")
    private java.lang.String keyEnterprisesOrgName;
	/**发证单位*/
	@Excel(name = "发证单位", width = 15)
    @ApiModelProperty(value = "发证单位")
    private java.lang.String keyEnterprisesTime;
	/**主管会计*/
	@Excel(name = "主管会计", width = 15)
    @ApiModelProperty(value = "主管会计")
    private java.lang.String accountingName;
	/**会计证号*/
	@Excel(name = "会计证号", width = 15)
    @ApiModelProperty(value = "会计证号")
    private java.lang.String accountingNo;
	/**统计员*/
	@Excel(name = "统计员", width = 15)
    @ApiModelProperty(value = "统计员")
    private java.lang.String statisticianName;
	/**统计员证号*/
	@Excel(name = "统计员证号", width = 15)
    @ApiModelProperty(value = "统计员证号")
    private java.lang.String statisticianNo;
	/**是否有研究机构*/
	@Excel(name = "是否有研究机构", width = 15)
    @ApiModelProperty(value = "是否有研究机构")
    private java.lang.Integer isResearch;
	/**是否有新产品开发中心*/
	@Excel(name = "是否有新产品开发中心", width = 15)
    @ApiModelProperty(value = "是否有新产品开发中心")
    private java.lang.Integer isDevelopmentCentre;
	/**民营企业发证时间*/
	@Excel(name = "民营企业发证时间", width = 15)
    @ApiModelProperty(value = "民营企业发证时间")
    private java.lang.String privateEnterpriseTime;
	/**民营企业发证批文*/
	@Excel(name = "民营企业发证批文", width = 15)
    @ApiModelProperty(value = "民营企业发证批文")
    private java.lang.String privateEnterpriseNo;
	/**年销售额*/
	@Excel(name = "年销售额", width = 15)
    @ApiModelProperty(value = "年销售额")
    private java.lang.String sales;
	/**单位简介*/
	@Excel(name = "单位简介", width = 15)
    @ApiModelProperty(value = "单位简介")
    private java.lang.String orgDescribe;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新时间")
    private java.util.Date updateTime;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建时间")
    private java.util.Date createTime;
	/**删除标记*/
	@Excel(name = "删除标记", width = 15)
    @ApiModelProperty(value = "删除标记")
    private java.lang.Integer delFlag;
	/**人员架构*/
	@Excel(name = "人员架构", width = 15)
    @ApiModelProperty(value = "人员架构")
    private java.lang.String personnelInfo;
	/**装备情况*/
	@Excel(name = "装备情况", width = 15)
    @ApiModelProperty(value = "装备情况")
    private java.lang.String equipmentInfo;
	/**经营情况*/
	@Excel(name = "经营情况", width = 15)
    @ApiModelProperty(value = "经营情况")
    private java.lang.String businessInfo;
	/**产品情况*/
	@Excel(name = "产品情况", width = 15)
    @ApiModelProperty(value = "产品情况")
    private java.lang.String productInfo;
}
