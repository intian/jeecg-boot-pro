package org.jeecg.modules.share.umsEnterprise.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.share.umsEnterprise.entity.UmsEnterprise;
import org.jeecg.modules.share.umsEnterprise.service.IUmsEnterpriseService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: ums_enterprise
 * @Author: jeecg-boot
 * @Date:   2022-01-19
 * @Version: V1.0
 */
@Api(tags="ums_enterprise")
@RestController
@RequestMapping("/umsEnterprise/umsEnterprise")
@Slf4j
public class UmsEnterpriseController extends JeecgController<UmsEnterprise, IUmsEnterpriseService> {
	@Autowired
	private IUmsEnterpriseService umsEnterpriseService;
	
	/**
	 * 分页列表查询
	 *
	 * @param umsEnterprise
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "ums_enterprise-分页列表查询")
	@ApiOperation(value="ums_enterprise-分页列表查询", notes="ums_enterprise-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(UmsEnterprise umsEnterprise,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<UmsEnterprise> queryWrapper = QueryGenerator.initQueryWrapper(umsEnterprise, req.getParameterMap());
		Page<UmsEnterprise> page = new Page<UmsEnterprise>(pageNo, pageSize);
		IPage<UmsEnterprise> pageList = umsEnterpriseService.page(page, queryWrapper);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param umsEnterprise
	 * @return
	 */
	@AutoLog(value = "ums_enterprise-添加")
	@ApiOperation(value="ums_enterprise-添加", notes="ums_enterprise-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody UmsEnterprise umsEnterprise) {
		umsEnterpriseService.save(umsEnterprise);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param umsEnterprise
	 * @return
	 */
	@AutoLog(value = "ums_enterprise-编辑")
	@ApiOperation(value="ums_enterprise-编辑", notes="ums_enterprise-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody UmsEnterprise umsEnterprise) {
		umsEnterpriseService.updateById(umsEnterprise);
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "ums_enterprise-通过id删除")
	@ApiOperation(value="ums_enterprise-通过id删除", notes="ums_enterprise-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		umsEnterpriseService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "ums_enterprise-批量删除")
	@ApiOperation(value="ums_enterprise-批量删除", notes="ums_enterprise-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.umsEnterpriseService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "ums_enterprise-通过id查询")
	@ApiOperation(value="ums_enterprise-通过id查询", notes="ums_enterprise-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		UmsEnterprise umsEnterprise = umsEnterpriseService.getById(id);
		if(umsEnterprise==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(umsEnterprise);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param umsEnterprise
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, UmsEnterprise umsEnterprise) {
        return super.exportXls(request, umsEnterprise, UmsEnterprise.class, "ums_enterprise");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, UmsEnterprise.class);
    }

}
