package org.jeecg.modules.share.umsProjectCategory.service.impl;

import org.jeecg.modules.share.umsProjectCategory.entity.UmsProjectCategory;
import org.jeecg.modules.share.umsProjectCategory.mapper.UmsProjectCategoryMapper;
import org.jeecg.modules.share.umsProjectCategory.service.IUmsProjectCategoryService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: ums_project_category
 * @Author: jeecg-boot
 * @Date:   2022-01-19
 * @Version: V1.0
 */
@Service
public class UmsProjectCategoryServiceImpl extends ServiceImpl<UmsProjectCategoryMapper, UmsProjectCategory> implements IUmsProjectCategoryService {

}
