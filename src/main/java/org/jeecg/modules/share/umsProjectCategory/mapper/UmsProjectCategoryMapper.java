package org.jeecg.modules.share.umsProjectCategory.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.share.umsProjectCategory.entity.UmsProjectCategory;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: ums_project_category
 * @Author: jeecg-boot
 * @Date:   2022-01-19
 * @Version: V1.0
 */
public interface UmsProjectCategoryMapper extends BaseMapper<UmsProjectCategory> {

}
