package org.jeecg.modules.share.umsProjectCategory.service;

import org.jeecg.modules.share.umsProjectCategory.entity.UmsProjectCategory;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: ums_project_category
 * @Author: jeecg-boot
 * @Date:   2022-01-19
 * @Version: V1.0
 */
public interface IUmsProjectCategoryService extends IService<UmsProjectCategory> {

}
