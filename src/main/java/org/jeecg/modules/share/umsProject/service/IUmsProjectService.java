package org.jeecg.modules.share.umsProject.service;

import org.jeecg.modules.share.umsProject.entity.UmsProject;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: ums_project
 * @Author: jeecg-boot
 * @Date:   2022-01-19
 * @Version: V1.0
 */
public interface IUmsProjectService extends IService<UmsProject> {

}
