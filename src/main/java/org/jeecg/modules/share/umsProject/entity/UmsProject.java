package org.jeecg.modules.share.umsProject.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: ums_project
 * @Author: jeecg-boot
 * @Date:   2022-01-19
 * @Version: V1.0
 */
@Data
@TableName("ums_project")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="ums_project对象", description="ums_project")
public class UmsProject implements Serializable {
    private static final long serialVersionUID = 1L;

	/**ID*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "ID")
    private java.lang.String id;
	/**项目类别ID*/
	@Excel(name = "项目类别ID", width = 15)
    @ApiModelProperty(value = "项目类别ID")
    private java.lang.String categoryId;
	/**项目类别名称*/
	@Excel(name = "项目类别名称", width = 15)
    @ApiModelProperty(value = "项目类别名称")
    private java.lang.String categoryName;
	/**项目名称*/
	@Excel(name = "项目名称", width = 15)
    @ApiModelProperty(value = "项目名称")
    private java.lang.String name;
	/**年度*/
	@Excel(name = "年度", width = 15)
    @ApiModelProperty(value = "年度")
    private java.lang.String year;
	/**单位名称*/
	@Excel(name = "单位名称", width = 15)
    @ApiModelProperty(value = "单位名称")
    private java.lang.String orgName;
	/**单位ID*/
	@Excel(name = "单位ID", width = 15)
    @ApiModelProperty(value = "单位ID")
    private java.lang.String orgId;
	/**申报人ID*/
	@Excel(name = "申报人ID", width = 15)
    @ApiModelProperty(value = "申报人ID")
    private java.lang.String userId;
	/**技术领域*/
	@Excel(name = "技术领域", width = 15)
    @ApiModelProperty(value = "技术领域")
    private java.lang.String researchField;
	/**推荐单位名称*/
	@Excel(name = "推荐单位名称", width = 15)
    @ApiModelProperty(value = "推荐单位名称")
    private java.lang.String recommendOrgName;
	/**推荐单位ID*/
	@Excel(name = "推荐单位ID", width = 15)
    @ApiModelProperty(value = "推荐单位ID")
    private java.lang.String recommendOrgId;
	/**提交时间*/
	@Excel(name = "提交时间", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "提交时间")
    private java.util.Date pushTime;
	/**项目状态*/
	@Excel(name = "项目状态", width = 15)
    @ApiModelProperty(value = "项目状态")
    private java.lang.Integer status;
}
