package org.jeecg.modules.share.umsProject.service.impl;

import org.jeecg.modules.share.umsProject.entity.UmsProject;
import org.jeecg.modules.share.umsProject.mapper.UmsProjectMapper;
import org.jeecg.modules.share.umsProject.service.IUmsProjectService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: ums_project
 * @Author: jeecg-boot
 * @Date:   2022-01-19
 * @Version: V1.0
 */
@Service
public class UmsProjectServiceImpl extends ServiceImpl<UmsProjectMapper, UmsProject> implements IUmsProjectService {

}
