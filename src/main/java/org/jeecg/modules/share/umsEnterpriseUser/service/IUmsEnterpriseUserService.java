package org.jeecg.modules.share.umsEnterpriseUser.service;

import org.jeecg.modules.share.umsEnterpriseUser.entity.UmsEnterpriseUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: ums_enterprise_user
 * @Author: jeecg-boot
 * @Date:   2022-01-19
 * @Version: V1.0
 */
public interface IUmsEnterpriseUserService extends IService<UmsEnterpriseUser> {

}
