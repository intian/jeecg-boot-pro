package org.jeecg.modules.share.umsExpert.entity.vo;

import lombok.Data;

/**
 * @Author werdor
 * @Date 2022/1/6 3:56 下午
 **/
//    {
//        "author":"张淞科",
//            "paperTitle":"关于研究o2o项目论文",
//            "source":"知网",
//            "publishTime":"2021-06-20",
//            "type":"系统"
//    }
@Data
public class ResearchResultNode {
    /**
     * 作者
     */
    private String author;
    /**
     * 成果或论文名称
     */
    private String paperTitle;
    /**
     * 来源
     */
    private String source;
    /**
     * 出版日期
     */
    private String publishTime;
    /**
     * 类别
     */
    private String type;
}
