package org.jeecg.modules.share.umsEnterprise.service;

import org.jeecg.modules.share.umsEnterprise.entity.UmsEnterprise;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: ums_enterprise
 * @Author: jeecg-boot
 * @Date:   2022-01-19
 * @Version: V1.0
 */
public interface IUmsEnterpriseService extends IService<UmsEnterprise> {

}
