package org.jeecg.modules.share.umsUser.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.share.umsUser.entity.UmsUser;

import java.util.Map;

/**
 * @Description: ums_user
 * @Author: wirdor
 * @Date:   2022-01-05
 * @Version: V1.0
 */
public interface IUmsUserService extends IService<UmsUser> {

    /**
     * 检查用户是否有效
     * @param umsUser
     * @return
     */
    Result checkUserIsEffective(UmsUser umsUser);

    /**
     * 根据不同用户类型查询是否能够注册
     * @param map
     * @return
     */
    Result checkUser(Map<String,Object> map);

    /**
     * 保存用户信息
     * @param salt 盐
     * @param username 用户名
     * @param passwordEncode 加密后的密码
     * @param userType 用户类型
     * @param extendId 额外ID
     * @param password 密码
     */
    void saveUsr(String salt,String username,String passwordEncode,Integer userType,String extendId,String password);


    String  saveUsrNoPaw(String username,Integer userType,String extendId);

    IPage<UmsUser> queryByOrgId(String orgId, Page<UmsUser> page);
}
