package org.jeecg.modules.share.umsEnterprise.service.impl;

import org.jeecg.modules.share.umsEnterprise.entity.UmsEnterprise;
import org.jeecg.modules.share.umsEnterprise.mapper.UmsEnterpriseMapper;
import org.jeecg.modules.share.umsEnterprise.service.IUmsEnterpriseService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: ums_enterprise
 * @Author: jeecg-boot
 * @Date:   2022-01-19
 * @Version: V1.0
 */
@Service
public class UmsEnterpriseServiceImpl extends ServiceImpl<UmsEnterpriseMapper, UmsEnterprise> implements IUmsEnterpriseService {

}
