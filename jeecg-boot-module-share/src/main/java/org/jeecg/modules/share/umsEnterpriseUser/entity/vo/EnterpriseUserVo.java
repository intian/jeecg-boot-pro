package org.jeecg.modules.share.umsEnterpriseUser.entity.vo;


import lombok.Data;

@Data
public class EnterpriseUserVo {

    /**
     * 姓名
     */
    private String name;

    /**
     * 密码
     */
    private String password;


    /**
     * 手机号（登陆账户)
     */
    private String phone;
}
