package org.jeecg.modules.share.umsExpert.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.share.umsExpert.entity.UmsExpert;

/**
 * @Description: ums_expert
 * @Author: jeecg-boot
 * @Date: 2022-01-19
 * @Version: V1.0
 */
public interface UmsExpertMapper extends BaseMapper<UmsExpert> {

}
