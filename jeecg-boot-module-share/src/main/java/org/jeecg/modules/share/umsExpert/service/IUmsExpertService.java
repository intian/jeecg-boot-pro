package org.jeecg.modules.share.umsExpert.service;

import org.jeecg.modules.share.umsExpert.entity.UmsExpert;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: ums_expert
 * @Author: jeecg-boot
 * @Date:   2022-01-19
 * @Version: V1.0
 */
public interface IUmsExpertService extends IService<UmsExpert> {

}
