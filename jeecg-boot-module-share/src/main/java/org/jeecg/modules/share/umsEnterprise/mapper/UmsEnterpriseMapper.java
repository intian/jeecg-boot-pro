package org.jeecg.modules.share.umsEnterprise.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.share.umsEnterprise.entity.UmsEnterprise;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: ums_enterprise
 * @Author: jeecg-boot
 * @Date:   2022-01-19
 * @Version: V1.0
 */
public interface UmsEnterpriseMapper extends BaseMapper<UmsEnterprise> {

}
