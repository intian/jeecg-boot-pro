package org.jeecg.modules.share.umsRecommend.service.impl;

import org.jeecg.modules.share.umsRecommend.entity.UmsRecommend;
import org.jeecg.modules.share.umsRecommend.mapper.UmsRecommendMapper;
import org.jeecg.modules.share.umsRecommend.service.IUmsRecommendService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: ums_recommend
 * @Author: jeecg-boot
 * @Date:   2022-01-19
 * @Version: V1.0
 */
@Service
public class UmsRecommendServiceImpl extends ServiceImpl<UmsRecommendMapper, UmsRecommend> implements IUmsRecommendService {

}
