package org.jeecg.modules.share.umsProject.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @Description: ums_project
 * @Author: jeecg-boot
 * @Date:   2022-01-19
 * @Version: V1.0
 */
@Data
@TableName("ums_project")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class UmsProject implements Serializable {
    private static final long serialVersionUID = 1L;

	/**ID*/
	@TableId(type = IdType.ASSIGN_ID)
    private String id;
	/**项目类别ID*/
	@Excel(name = "项目类别ID", width = 15)
    private String categoryId;
	/**项目类别名称*/
	@Excel(name = "项目类别名称", width = 15)
    private String categoryName;
	/**项目名称*/
	@Excel(name = "项目名称", width = 15)
    private String name;
	/**年度*/
	@Excel(name = "年度", width = 15)
    private String year;
	/**单位名称*/
	@Excel(name = "单位名称", width = 15)
    private String orgName;
	/**单位ID*/
	@Excel(name = "单位ID", width = 15)
    private String orgId;
	/**申报人ID*/
	@Excel(name = "申报人ID", width = 15)
    private String userId;
	/**技术领域*/
	@Excel(name = "技术领域", width = 15)
    private String researchField;
	/**推荐单位名称*/
	@Excel(name = "推荐单位名称", width = 15)
    private String recommendOrgName;
	/**推荐单位ID*/
	@Excel(name = "推荐单位ID", width = 15)
    private String recommendOrgId;
	/**提交时间*/
	@Excel(name = "提交时间", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date pushTime;
	/**项目状态*/
	@Excel(name = "项目状态", width = 15)
    private Integer status;
}
