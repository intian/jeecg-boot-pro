package org.jeecg.modules.share.umsEnterprise.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @Description: ums_enterprise
 * @Author: jeecg-boot
 * @Date:   2022-01-19
 * @Version: V1.0
 */
@Data
@TableName("ums_enterprise")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class UmsEnterprise implements Serializable {
    private static final long serialVersionUID = 1L;

	/**ID*/
	@TableId(type = IdType.ASSIGN_ID)
    private String id;
	/**企业姓名*/
	@Excel(name = "企业姓名", width = 15)
    private String orgName;
	/**统一信用代码*/
	@Excel(name = "统一信用代码", width = 15)
    private String creditCode;
	/**法人姓名*/
	@Excel(name = "法人姓名", width = 15)
    private String legalName;
	/**电子邮箱*/
	@Excel(name = "电子邮箱", width = 15)
    private String email;
	/**联系电话*/
	@Excel(name = "联系电话", width = 15)
    private String phone;
	/**推荐单位*/
	@Excel(name = "推荐单位", width = 15)
    private String recommendOrg;
	/**注册类型*/
	@Excel(name = "注册类型", width = 15)
    private String registerType;
	/**注册资本*/
	@Excel(name = "注册资本", width = 15)
    private String registerCapital;
	/**注册时间*/
	@Excel(name = "注册时间", width = 15)
    private String registerTime;
	/**单位地址*/
	@Excel(name = "单位地址", width = 15)
    private String address;
	/**邮政编码*/
	@Excel(name = "邮政编码", width = 15)
    private String postalCode;
	/**网址*/
	@Excel(name = "网址", width = 15)
    private String website;
	/**单位性质*/
	@Excel(name = "单位性质", width = 15)
    private String orgNature;
	/**传真*/
	@Excel(name = "传真", width = 15)
    private String fax;
	/**是否为高新企业*/
	@Excel(name = "是否为高新企业", width = 15)
    private Integer isHighTech;
	/**职工人数*/
	@Excel(name = "职工人数", width = 15)
    private String num;
	/**高新企业批文号*/
	@Excel(name = "高新企业批文号", width = 15)
    private String highTechNo;
	/**高新企业批文时间*/
	@Excel(name = "高新企业批文时间", width = 15)
    private String highTechTime;
	/**所属行业*/
	@Excel(name = "所属行业", width = 15)
    private String industry;
	/**龙头企业发证单位*/
	@Excel(name = "龙头企业发证单位", width = 15)
    private String keyEnterprisesOrgName;
	/**发证单位*/
	@Excel(name = "发证单位", width = 15)
    private String keyEnterprisesTime;
	/**主管会计*/
	@Excel(name = "主管会计", width = 15)
    private String accountingName;
	/**会计证号*/
	@Excel(name = "会计证号", width = 15)
    private String accountingNo;
	/**统计员*/
	@Excel(name = "统计员", width = 15)
    private String statisticianName;
	/**统计员证号*/
	@Excel(name = "统计员证号", width = 15)
    private String statisticianNo;
	/**是否有研究机构*/
	@Excel(name = "是否有研究机构", width = 15)
    private Integer isResearch;
	/**是否有新产品开发中心*/
	@Excel(name = "是否有新产品开发中心", width = 15)
    private Integer isDevelopmentCentre;
	/**民营企业发证时间*/
	@Excel(name = "民营企业发证时间", width = 15)
    private String privateEnterpriseTime;
	/**民营企业发证批文*/
	@Excel(name = "民营企业发证批文", width = 15)
    private String privateEnterpriseNo;
	/**年销售额*/
	@Excel(name = "年销售额", width = 15)
    private String sales;
	/**单位简介*/
	@Excel(name = "单位简介", width = 15)
    private String orgDescribe;
	/**更新人*/
    private String updateBy;
	/**更新时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date updateTime;
	/**创建人*/
    private String createBy;
	/**创建时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date createTime;
	/**删除标记*/
	@Excel(name = "删除标记", width = 15)
    private Integer delFlag;
	/**人员架构*/
	@Excel(name = "人员架构", width = 15)
    private String personnelInfo;
	/**装备情况*/
	@Excel(name = "装备情况", width = 15)
    private String equipmentInfo;
	/**经营情况*/
	@Excel(name = "经营情况", width = 15)
    private String businessInfo;
	/**产品情况*/
	@Excel(name = "产品情况", width = 15)
    private String productInfo;
}
