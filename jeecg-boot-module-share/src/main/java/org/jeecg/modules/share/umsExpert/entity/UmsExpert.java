package org.jeecg.modules.share.umsExpert.entity;

import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.handlers.FastjsonTypeHandler;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecg.modules.share.umsExpert.entity.vo.ProjectResultNode;
import org.jeecg.modules.share.umsExpert.entity.vo.ResearchResultNode;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @Description: ums_expert
 * @Author: jeecg-boot
 * @Date: 2022-01-05
 * @Version: V1.0
 */
@Data
@TableName("ums_expert")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class UmsExpert implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * ID
	 */
	@TableId(type = IdType.ASSIGN_ID)
	private String id;
	/**
	 * 专家姓名
	 */
	@Excel(name = "专家姓名", width = 15)
	private String realname;
	/**
	 * 性别
	 */
	@Excel(name = "性别", width = 15)
	private Integer sex;
	/**
	 * 头像
	 */
	@Excel(name = "头像", width = 15)
	private String avatar;
	/**
	 * 单位性质
	 */
	@Excel(name = "单位性质", width = 15)
	private String orgNature;
	/**
	 * 办公地点
	 */
	@Excel(name = "办公地点", width = 15)
	private String orgAddress;
	/**
	 * 办公电话
	 */
	@Excel(name = "办公电话", width = 15)
	private String orgTel;
	/**
	 * 职务
	 */
	@Excel(name = "职务", width = 15)
	private String jobs;
	/**
	 * 职称
	 */
	@Excel(name = "职称", width = 15)
	private String post;
	/**
	 * 邮箱
	 */
	@Excel(name = "邮箱", width = 15)
	private String email;
	/**
	 * 手机号
	 */
	@Excel(name = "手机号", width = 15)
	private String phone;
	/**
	 * 证件号码
	 */
	@Excel(name = "证件号码", width = 15)
	private String idCard;
	/**
	 * 身份类型
	 */
	@Excel(name = "身份类型", width = 15)
	private Integer idType;
	/**
	 * 学历
	 */
	@Excel(name = "学历", width = 15)
	private String education;
	/**
	 * 学位
	 */
	@Excel(name = "学位", width = 15)
	private String degree;
	/**
	 * 单位名称
	 */
	@Excel(name = "单位名称", width = 15)
	private String orgName;
	/**
	 * 籍贯
	 */
	@Excel(name = "籍贯", width = 15)
	private String nativePlace;
	/**
	 * 专家类型
	 */
	@Excel(name = "专家类型", width = 15)
	private String expertType;
	/**
	 * 研究方向
	 */
	@Excel(name = "研究方向", width = 15)
	private String researchDirection;
	/**
	 * 开户银行
	 */
	@Excel(name = "开户银行", width = 15)
	private String bankName;
	/**
	 * 开户账号
	 */
	@Excel(name = "开户账号", width = 15)
	private String bankAccount;
	/**
	 * 个人履历
	 */
	@Excel(name = "个人履历", width = 15)
	private String resume;
	/**
	 * 研究领域
	 */
	@Excel(name = "研究领域", width = 15)
	@TableField(typeHandler = FastjsonTypeHandler.class)
	private JSONArray researchField;
	/**
	 * 专业关键词
	 */
	@Excel(name = "专业关键词", width = 15)
	private String majorKeyword;
	/**
	 * 参与评审兴趣度
	 */
	@Excel(name = "参与评审兴趣度", width = 15)
	private String reviewScore;
	/**
	 * 研究成果
	 */
	@Excel(name = "研究成果", width = 15)
	@TableField(typeHandler = FastjsonTypeHandler.class)
	private List<ResearchResultNode> researchResult;
	/**
	 * 承担项目情况
	 */
	@Excel(name = "承担项目情况", width = 15)
	@TableField(typeHandler = FastjsonTypeHandler.class)
	private List<ProjectResultNode> projectResult;
	/**
	 * 审核状态
	 */
	@Excel(name = "审核状态", width = 15)
	private Integer status;
	/**
	 * 创建人登录名称
	 */
	private String createBy;
	/**
	 * 创建日期
	 */
	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date createTime;
	/**
	 * 更新人登录名称
	 */
	private String updateBy;
	/**
	 * 更新日期
	 */
	@JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date updateTime;
	/**
	 * 删除状态(0-正常,1-已删除)
	 */
	@Excel(name = "删除状态(0-正常,1-已删除)", width = 15)
	private Integer delFlag;

	/**
	 * 生日
	 */
	@Excel(name = "生日", width = 15)
	private String birthday;
}
