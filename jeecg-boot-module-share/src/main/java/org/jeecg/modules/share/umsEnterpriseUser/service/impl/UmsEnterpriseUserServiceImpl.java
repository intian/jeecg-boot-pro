package org.jeecg.modules.share.umsEnterpriseUser.service.impl;

import org.jeecg.modules.share.umsEnterpriseUser.entity.UmsEnterpriseUser;
import org.jeecg.modules.share.umsEnterpriseUser.mapper.UmsEnterpriseUserMapper;
import org.jeecg.modules.share.umsEnterpriseUser.service.IUmsEnterpriseUserService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: ums_enterprise_user
 * @Author: jeecg-boot
 * @Date:   2022-01-19
 * @Version: V1.0
 */
@Service
public class UmsEnterpriseUserServiceImpl extends ServiceImpl<UmsEnterpriseUserMapper, UmsEnterpriseUser> implements IUmsEnterpriseUserService {

}
