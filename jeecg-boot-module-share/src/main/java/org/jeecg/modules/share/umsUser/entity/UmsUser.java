package org.jeecg.modules.share.umsUser.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

/**
 * @Description: ums_user
 * @Author: jeecg-boot
 * @Date:   2022-01-05
 * @Version: V1.0
 */
@Data
@TableName("ums_user")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class UmsUser implements Serializable {
    private static final long serialVersionUID = 1L;

	/**ID*/
	@TableId(type = IdType.ASSIGN_ID)
    private String id;
	/**用户名*/
	@Excel(name = "用户名", width = 15)
    private String username;
	/**密码*/
	@Excel(name = "密码", width = 15)
    private String password;
	/**md5密码盐*/
	@Excel(name = "md5密码盐", width = 15)
    private String salt;
	/**用户类型 1.企业 2.专家 3.推荐单位*/
	@Excel(name = "用户类型", width = 15)
    private Integer type;
	/**拓展信息ID*/
	@Excel(name = "拓展信息ID", width = 15)
    private String extendId;
	/**创建人登录名称*/
    private String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date createTime;
	/**更新人登录名称*/
    private String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date updateTime;
	/**删除状态(0-正常,1-已删除)*/
	@Excel(name = "删除状态(0-正常,1-已删除)", width = 15)
    private Integer delFlag;
}
