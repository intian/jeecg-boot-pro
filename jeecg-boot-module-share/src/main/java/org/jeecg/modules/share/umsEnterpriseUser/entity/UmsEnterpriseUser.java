package org.jeecg.modules.share.umsEnterpriseUser.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecg.modules.share.umsEnterprise.entity.UmsEnterprise;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @Description: ums_enterprise_user
 * @Author: jeecg-boot
 * @Date:   2022-01-19
 * @Version: V1.0
 */
@Data
@TableName("ums_enterprise_user")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class UmsEnterpriseUser implements Serializable {
    private static final long serialVersionUID = 1L;

	/**ID*/
	@TableId(type = IdType.ASSIGN_ID)
    private String id;
	/**企业姓名*/
	@Excel(name = "企业姓名", width = 15)
    private String orgName;
	/**企业ID*/
	@Excel(name = "企业ID", width = 15)
    private String orgId;
	/**姓名*/
	@Excel(name = "姓名", width = 15)
    private String name;
	/**联系电话*/
	@Excel(name = "联系电话", width = 15)
    private String phone;
	/**电话*/
	@Excel(name = "电话", width = 15)
    private String tel;
	/**邮箱*/
	@Excel(name = "邮箱", width = 15)
	private String email;
	/**更新人*/
    private String updateBy;
	/**更新时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date updateTime;
	/**创建人*/
    private String createBy;
	/**创建时间*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date createTime;
	/**删除标记*/
	@Excel(name = "删除标记", width = 15)
    private Integer delFlag;

	@TableField(exist = false)
	private UmsEnterprise umsEnterprise;
}
