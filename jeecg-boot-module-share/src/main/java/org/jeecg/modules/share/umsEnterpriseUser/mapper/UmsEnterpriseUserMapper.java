package org.jeecg.modules.share.umsEnterpriseUser.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.share.umsEnterpriseUser.entity.UmsEnterpriseUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: ums_enterprise_user
 * @Author: jeecg-boot
 * @Date:   2022-01-19
 * @Version: V1.0
 */
public interface UmsEnterpriseUserMapper extends BaseMapper<UmsEnterpriseUser> {

}
