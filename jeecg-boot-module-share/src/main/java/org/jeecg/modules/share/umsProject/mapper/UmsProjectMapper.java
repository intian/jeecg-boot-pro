package org.jeecg.modules.share.umsProject.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.share.umsProject.entity.UmsProject;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: ums_project
 * @Author: jeecg-boot
 * @Date:   2022-01-19
 * @Version: V1.0
 */
public interface UmsProjectMapper extends BaseMapper<UmsProject> {

}
