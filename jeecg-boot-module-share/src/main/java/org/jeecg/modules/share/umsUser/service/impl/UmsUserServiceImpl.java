package org.jeecg.modules.share.umsUser.service.impl;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.thread.ThreadUtil;
import cn.hutool.extra.mail.MailUtil;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.constant.CommonConstant;
import org.jeecg.common.constant.OrgTypeConstant;
import org.jeecg.modules.share.umsUser.entity.UmsUser;
import org.jeecg.modules.share.umsUser.mapper.UmsUserMapper;
import org.jeecg.modules.share.umsUser.service.IUmsUserService;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.Map;

/**
 * @Description: ums_user
 * @Author: jeecg-boot
 * @Date:   2022-01-05
 * @Version: V1.0
 */
@Service
public class UmsUserServiceImpl extends ServiceImpl<UmsUserMapper, UmsUser> implements IUmsUserService {

    @Override
    public Result checkUserIsEffective(UmsUser umsUser) {
        Result<?> result = new Result<Object>();
        //情况1：根据用户信息查询，该用户不存在
        if (umsUser == null) {
            result.error500("该用户不存在，请注册");
            return result;
        }
        //情况2：根据用户信息查询，该用户已注销
        //update-begin---author:王帅   Date:20200601  for：if条件永远为falsebug------------
        if (CommonConstant.DEL_FLAG_1.equals(umsUser.getDelFlag())) {
            //update-end---author:王帅   Date:20200601  for：if条件永远为falsebug------------
            result.error500("该用户已注销");
            return result;
        }
        //情况3：根据用户信息查询，该用户已冻结
//        if (CommonConstant.USER_FREEZE.equals(umsUser.getStatus())) {
//            result.error500("该用户已冻结");
//            return result;
//        }
        return result;
    }

    @Override
    public Result checkUser(Map<String,Object> map) {
        Result<?> result = new Result<>();
        String username = Convert.toStr(map.get("email"));
        LambdaQueryWrapper<UmsUser> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(UmsUser::getUsername, username);
        UmsUser umsUser = this.getOne(queryWrapper);
        if (umsUser!=null) {
            result.error500("该用户已存在，请勿重复注册");
            return result;
        }
        return result;
    }

    @Override
    @Transactional
    public void saveUsr(String salt,String username,String passwordEncode,Integer userType,String extendId,String password) {
        UmsUser umsUser = new UmsUser();
        umsUser.setSalt(salt);
        umsUser.setUsername(username);
        umsUser.setPassword(passwordEncode);
        umsUser.setType(userType);
        umsUser.setDelFlag(CommonConstant.DEL_FLAG_0);
        umsUser.setCreateTime(new Date());
        umsUser.setExtendId(extendId);
        this.save(umsUser);
        //给专家发生异步消息 生成的密码发给专家
        if (OrgTypeConstant.ORG_TYPE_2.equals(userType)){
            sendMail(password);
        }
    }

    @Override
    public String saveUsrNoPaw(String username, Integer userType, String extendId) {
        UmsUser umsUser = new UmsUser();
        umsUser.setUsername(username);
        umsUser.setType(userType);
        umsUser.setDelFlag(CommonConstant.DEL_FLAG_0);
        umsUser.setCreateTime(new Date());
        umsUser.setExtendId(extendId);
        this.save(umsUser);
        return umsUser.getId();

    }

    @Override
    public IPage<UmsUser> queryByOrgId(String orgId, Page<UmsUser> page) {
       return baseMapper.queryByOrgId(page,orgId);
    }

    @Async("asyncTaskExecutor")
    public void  sendMail (String password){
        //todo 模拟发送信息
        ThreadUtil.execAsync(()->{
            MailUtil.send("179946422@qq.com", "常德市科技局注册信息", "生成密码："+password, false);
        });
    }
}
