package org.jeecg.modules.share.umsUser.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.share.umsUser.entity.UmsUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: ums_user
 * @Author: jeecg-boot
 * @Date:   2022-01-05
 * @Version: V1.0
 */
public interface UmsUserMapper extends BaseMapper<UmsUser> {

    IPage<UmsUser> queryByOrgId(Page<UmsUser> page, String orgId);
}
