package org.jeecg.modules.share.umsExpert.entity.vo;

import lombok.Data;

/**
 * @Author werdor
 * @Date 2022/1/6 4:06 下午
 **/
@Data
public class ProjectResultNode {

    /**
     * 项目编号
     */
    private String projectNo;

    /**
     * 项目名称
     */
    private String projectName;

    /**
     * 项目状态
     */
    private String projectStatus;

    /**
     * 项目年度
     */
    private String projectYear;

    /**
     * 本人分工
     */
    private String division;

    /**
     * 开始日期
     */
    private String startTime;

    /**
     * 结束日期
     */
    private String endTime;
}
