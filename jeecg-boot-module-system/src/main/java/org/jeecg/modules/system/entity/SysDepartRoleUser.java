package org.jeecg.modules.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;

/**
 * @Description: 部门角色人员信息
 * @Author: jeecg-boot
 * @Date: 2020-02-13
 * @Version: V1.0
 */
@Data
@TableName("sys_depart_role_user")
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class SysDepartRoleUser {

    /**
     * 主键id
     */
    @TableId(type = IdType.ASSIGN_ID)

    private String id;
    /**
     * 用户id
     */
    @Excel(name = "用户id", width = 15)
    private String userId;
    /**
     * 角色id
     */
    @Excel(name = "角色id", width = 15)
    private String droleId;

    public SysDepartRoleUser() {

    }

    public SysDepartRoleUser(String userId, String droleId) {
        this.userId = userId;
        this.droleId = droleId;
    }
}
