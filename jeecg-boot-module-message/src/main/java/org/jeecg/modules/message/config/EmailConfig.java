package org.jeecg.modules.message.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @Author werdor
 * @Date 2021/12/23 1:37 上午
 **/
@Component
@Data
public class EmailConfig {

    @Value(value = "${spring.mail.username}")
    private String emailFrom;

}
